// Tested on MSVC and GCC
// Requires C++17
// Tip: Use -Wno-unknown-pragmas when compiling on GCC

#pragma warning(disable: 26440 26446 26438)

#ifndef _DEBUG
#  define NDEBUG // Turn off asserts
#endif

#include <cstdint>
#include <cassert>

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

using QuestionT = uint_fast8_t;
using ExamT = uint_fast8_t;

constexpr ExamT maxExams{12};
constexpr QuestionT maxQuestions{40};
static_assert(maxExams > 0 && maxQuestions > 0);

#pragma region Config

/* For testing on the server, make sure to disable:
 * - BENCHMARK
 * - DO_GENERATE
 * - TEST
 * - TRACK_SOLUTIONS
 *
 * And enable COMBINE_ORDER and PRUNE for maximal efficiency
 */

// Display durations
//#define BENCHMARK

// Generate random test case (don't read from stdin)
//#define DO_GENERATE

#ifdef DO_GENERATE
// Overrides generateExams and generateQuestions; repeat for [1, maxExams] and [1, maxQuestions]
//#define GENERATE_ALL_COMBINATIONS

#ifndef GENERATE_ALL_COMBINATIONS
// [1, maxExams]
constexpr ExamT generateExams{12};
// [1, maxQuestions]
constexpr QuestionT generateQuestions{40};
static_assert(generateExams > 0 && generateExams <= maxExams
	&& generateQuestions > 0 && generateQuestions <= maxQuestions);
#endif
// Specify a seed, or undefine for a time-based seed
#define GENERATE_SEED default_random_engine::default_seed
#endif // DO_GENERATE

// Collect all solutions; combine with TEST to check if solutions are really correct
// Not optimized for few students with many questions, and the optimized code will not be tested!
//#define TRACK_SOLUTIONS

// Compare solutions with full bruteforce
//#define TEST

// Don't print all solutions
#define TEST_DONT_PRINT_ALL

// Enable debug output (mostly for combining)
//#define DEBOUT

// Combine results into order heuristic; more efficient
#define COMBINE_ORDER

// Preliminarily prune some models during bruteforce
#define PRUNE

#pragma endregion




#if defined BENCHMARK || defined DO_GENERATE
#  include <chrono>
#endif

#ifdef DO_GENERATE
#  include <random>
#  include <sstream>
#endif

#ifdef DEBOUT
#  define IFDEB(stm) stm
#else
#  define IFDEB(stm)
#endif

#ifdef TRACK_SOLUTIONS
#  include <set>
#  define TRACK_ARG(arg) ,arg
#else
#  define TRACK_ARG(arg)
#endif

#if defined TRACK_SOLUTIONS && !defined COMBINE_ORDER
#  error TRACK_SOLUTIONS is not implemented without COMBINE_ORDER, sorry!
#endif

#ifdef COMBINE_ORDER
#  include <array>
#  define COMBINE_ARG(arg) ,arg
#  define NOCOMBINE_ARG(arg)
#  define COMBINE_TEMPL(arg) <arg>
#else
#  define COMBINE_ARG(arg)
#  define NOCOMBINE_ARG(arg) ,arg
#  define COMBINE_TEMPL(arg)
#endif


#ifdef _MSC_VER
#  include <intrin.h>
#endif

#pragma region popcount

#ifdef _MSC_VER
#  define popcount64 __popcnt64
#  define popcount32 __popcnt
#else
#  define popcount64 __builtin_popcountll
#  define popcount32 __builtin_popcount
static_assert(sizeof(unsigned) >= sizeof(uint32_t));
#endif

template<class NumType>
uint_fast8_t popcount(NumType);

template<>
uint_fast8_t popcount(const uint32_t num) { return static_cast<uint_fast8_t>(popcount32(num)); }

template<>
uint_fast8_t popcount(const uint64_t num) { return static_cast<uint_fast8_t>(popcount64(num)); }

#pragma endregion

#pragma region findFirstSet

#ifdef _MSC_VER
// Using _BitScanReverse64 is also an option, but it returns the index in a pointer
#  define lzcnt64 __lzcnt64
#else
#  define lzcnt64 __builtin_clzll
static_assert(sizeof(unsigned long long) == sizeof(uint64_t));
#endif

uint_fast8_t findFirstSet(const uint64_t num)
{
	assert(num);
	return static_cast<uint_fast8_t>(64 - lzcnt64(num) - 1);
}

constexpr uint_fast8_t findFirstSetConst(uint64_t num)
{
	assert(num);
	uint_fast8_t pos = 0;
	for (; num; num >>= 1, ++pos) {}
	return pos - 1;
}

#pragma endregion


#pragma region debugbreak

#ifdef _MSC_VER
#  define debugbreak __debugbreak
#else
#  define debugbreak __builtin_trap
#endif

#pragma endregion



#pragma region Answers

enum AnswersClass { Left = 0, Right, Full };

constexpr AnswersClass otherAnswersClass(const AnswersClass one)
{
	assert(one != Full);
	return static_cast<AnswersClass>(1 - one);
}

#pragma region AnswersType

template<AnswersClass Class>
struct _AnswersType;

template<>
struct _AnswersType<Full>
{
	using type = uint64_t;
};

template<>
struct _AnswersType<Left>
{
	using type = uint32_t;
};

template<>
struct _AnswersType<Right>
{
	using type = uint32_t;
};

template<AnswersClass Class>
using AnswersType = typename _AnswersType<Class>::type;

#pragma endregion


// Represents an answer set
template<AnswersClass Class>
class Answers
{
	static Answers mask_;

	static bool checkPos(const QuestionT rPos)
	{
		return rPos < nrQuestions;
	}

	AnswersType<Class> data_;
	static_assert(!numeric_limits<decltype(data_)>::is_signed);

	bool checkOverflow() const { return data_ <= mask_.data_; }

public:
	static QuestionT nrQuestions;

	// nrQuestions 1s starting at the right (LSB)
	static Answers mask() { return mask_; }

	// Prerequisite: nrQuestions
	static void updateMask()
	{
		if (nrQuestions < 8 * sizeof data_)
			mask_  = (static_cast<decltype(data_)>(1) << nrQuestions) - 1;
		else mask_ = numeric_limits<decltype(data_)>::max();
	}

	// Note that the LSB (rightmost) in data is seen as the first answer
	constexpr Answers(const decltype(data_) data = 0) noexcept : data_(data) {}

#pragma region Operators
	[[nodiscard]] explicit operator AnswersType<Class>() const { return data_; }

	auto& operator+=(const decltype(data_) x)
	{
		data_ += x;
		return *this;
	}

	auto& operator-=(const decltype(data_) x)
	{
		data_ -= x;
		return *this;
	}

	auto& operator<<=(const QuestionT places)
	{
		data_ <<= places;
		return *this;
	}

	auto& operator>>=(const QuestionT places)
	{
		data_ >>= places;
		return *this;
	}

	auto& operator&=(const Answers &other)
	{
		data_ &= other.data_;
		return *this;
	}

	auto& operator|=(const Answers &other)
	{
		data_ |= other.data_;
		return *this;
	}

	auto& operator^=(const Answers &other)
	{
		data_ ^= other.data_;
		return *this;
	}

	[[nodiscard]] Answers operator+(const decltype(data_) x) const { return data_ + x; }
	[[nodiscard]] Answers operator-(const decltype(data_) x) const { return data_ - x; }

	[[nodiscard]] Answers operator<<(const QuestionT places) const { return data_ << places; }
	[[nodiscard]] Answers operator>>(const QuestionT places) const { return data_ >> places; }

	[[nodiscard]] Answers operator&(const Answers other) const { return data_ & other.data_; }
	[[nodiscard]] Answers operator|(const Answers other) const { return data_ | other.data_; }
	[[nodiscard]] Answers operator^(const Answers other) const { return data_ ^ other.data_; }

	[[nodiscard]] Answers operator~() const { return ~data_ & mask_.data_; }

	auto& operator++() { return *this += 1; }
	auto& operator--() { return *this -= 1; }

	[[nodiscard]] auto operator++(int)
	{
		auto old = *this;
		++*this;
		return old;
	}

	[[nodiscard]] auto operator--(int)
	{
		auto old = *this;
		--*this;
		return old;
	}

	[[nodiscard]] bool operator<(const Answers other) const
	{
		assert(checkOverflow());
		return data_ < other.data_;
	}

	[[nodiscard]] bool operator<=(const Answers other) const
	{
		assert(checkOverflow());
		return data_ <= other.data_;
	}

	[[nodiscard]] bool operator>(const Answers other) const
	{
		assert(checkOverflow());
		return data_ > other.data_;
	}

	[[nodiscard]] bool operator>=(const Answers other) const
	{
		assert(checkOverflow());
		return data_ >= other.data_;
	}

	[[nodiscard]] bool operator==(const Answers other) const
	{
		assert(checkOverflow());
		return data_ == other.data_;
	}

	[[nodiscard]] bool operator!=(const Answers &other) const
	{
		assert(checkOverflow());
		return data_ != other.data_;
	}

#pragma endregion

	[[nodiscard]] auto data() const { return data_; }

	// Get bit at rPos places from the right
	[[nodiscard]]
	bool operator[](const QuestionT rPos) const
	{
		assert(checkPos(rPos));
		return data_ >> rPos & 1;
	}

	// Set bit (to 1) at rPos places from the right
	auto& set(const QuestionT rPos)
	{
		//assert(checkPos(rPos));
		data_ |= static_cast<decltype(data_)>(1) << rPos;
		return *this;
	}

	// Clear bit (to 0) at rPos places from the right
	auto& reset(const QuestionT rPos)
	{
		assert(checkPos(rPos));
		data_ &= ~(static_cast<decltype(data_)>(1) << rPos);
		return *this;
	}

#pragma region Misc

	// Number of set bits
	[[nodiscard]] QuestionT count() const
	{
		assert(checkOverflow());
		return popcount(data_);
	}

	// Are all bits set?
	[[nodiscard]] bool all() const
	{
		assert(checkOverflow());
		return data_ == mask_.data_;
	}

	// Are none of the bits set?
	[[nodiscard]] bool none() const
	{
		assert(checkOverflow());
		return !data_;
	}

	// Hamming distance
	[[nodiscard]]
	QuestionT differingBits(const Answers other) const { return (*this ^ other).count(); }

	[[nodiscard]]
	QuestionT commonBits(const Answers other) const { return nrQuestions - differingBits(other); }
#pragma endregion
};

template<AnswersClass Class>
QuestionT Answers<Class>::nrQuestions;

template<AnswersClass Class>
Answers<Class> Answers<Class>::mask_;

static_assert(numeric_limits<AnswersType<Full>>::digits >= maxQuestions);
static_assert(numeric_limits<AnswersType<Left>>::digits + numeric_limits<AnswersType<Right>>::digits
	>= maxQuestions);

template<AnswersClass Class>
auto& operator>>(istream &in, Answers<Class> &answers)
{
	in >> ws;
	for (QuestionT pos   = 0; pos < Answers<Class>::nrQuestions; ++pos)
		if (const auto c = in.get(); c == '1') answers.set(pos);
		else assert(c == '0');

	return in;
}

template<AnswersClass Class>
auto& operator<<(ostream &out, const Answers<Class> &answers)
{
	for (QuestionT pos = 0; pos < Answers<Class>::nrQuestions; ++pos)
		out << (answers[pos] ? '1' : '0');
	return out;
}


void splitAnswers(const Answers<Full> full, Answers<Left> &left, Answers<Right> &right)
{
	// Because of the reversed order, left is the right side of full seen bitwise
	left  = static_cast<AnswersType<Left>>((full & Answers<Left>::mask().data()).data());
	right = static_cast<AnswersType<Right>>((full >> Answers<Left>::nrQuestions).data());
}

[[nodiscard]]
Answers<Full> combineAnswers(const Answers<Left> left, const Answers<Right> right)
{
	return Answers<Full>(right.data()) << Answers<Left>::nrQuestions | Answers<Full>(left.data());
}

#pragma endregion


struct Exam
{
	// = number of students
	static ExamT nrExams;

	QuestionT questionsCorrect;
	Answers<Full> answers;
	Answers<Left> answersLeft;
	Answers<Right> answersRight;

	void updateHalves()
	{
		splitAnswers(answers, answersLeft, answersRight);
	}
#pragma warning(suppress: 26495)
};

ExamT Exam::nrExams;

#pragma region getAnswers

template<AnswersClass Class>
[[nodiscard]] Answers<Class>& getAnswers(Exam &);

template<>
Answers<Full>& getAnswers(Exam &exam) { return exam.answers; }

template<>
Answers<Left>& getAnswers(Exam &exam) { return exam.answersLeft; }

template<>
Answers<Right>& getAnswers(Exam &exam) { return exam.answersRight; }

template<AnswersClass Class>
[[nodiscard]] const Answers<Class>& getAnswers(const Exam &);

template<>
const Answers<Full>& getAnswers(const Exam &exam) { return exam.answers; }

template<>
const Answers<Left>& getAnswers(const Exam &exam) { return exam.answersLeft; }

template<>
const Answers<Right>& getAnswers(const Exam &exam) { return exam.answersRight; }

#pragma endregion

auto& operator>>(istream &in, Exam &exam)
{
	unsigned questionsCorrect;
	in >> exam.answers >> questionsCorrect;
	exam.questionsCorrect = static_cast<QuestionT>(questionsCorrect);
	exam.updateHalves();
	return in;
}


#ifdef COMBINE_ORDER

/**
 * Basically a heuristic
 * If left.correctPerExam + right.correctPerExam = goal.correctPerExam
 *	then left.order + right.order = goal.order MUST hold
 * left.order + right.order must not overflow
 * Orders SHOULD be as unique as possible
 */
class Order
{
	static constexpr auto maxBitsPerField = findFirstSetConst(maxQuestions) + 1;

	static ExamT maxFields_;
	static uint_fast8_t bitsPerField_;

	uint64_t data_;
	static_assert(!numeric_limits<decltype(data_)>::is_signed);

public:
	static constexpr auto minMaxFields = numeric_limits<decltype(data_)>::digits / maxBitsPerField;

	[[nodiscard]] static auto maxFields() { return maxFields_; }

	// Prerequisites: Exam::nrExams, Answers<Full>::nrQuestions
	static void initBitCounts()
	{
		bitsPerField_ = findFirstSet(Answers<Full>::nrQuestions) + 1;
		maxFields_    = numeric_limits<decltype(data_)>::digits / bitsPerField_;
	}

	void setFirstField(const uint_fast8_t val) { data_ = val; }

	void addField(const uint_fast8_t val)
	{
		(data_ <<= bitsPerField_) |= val;
	}

	operator decltype(data_)() const { return data_; }
};

ExamT Order::maxFields_;
uint_fast8_t Order::bitsPerField_;


template<AnswersClass Class>
struct QuestionsCorrectForModelBase
{
	Answers<Class> model;
	Order order;

	[[nodiscard]] bool operator<(const QuestionsCorrectForModelBase &other) const
	{
		return order < other.order;
	}
};

/**
 * \tparam NrChecks Size of correctPerRemainingExam; must be equal to max(0, Exam::nrExams - Order::maxFields())
 */
template<AnswersClass Class, ExamT NrChecks>
struct QuestionsCorrectForModel : QuestionsCorrectForModelBase<Class>
{
	// Only contains correct counts which have to be checked
	array<QuestionT, NrChecks> correctPerRemainingExam;
};

// No checks
template<AnswersClass Class>
struct QuestionsCorrectForModel<Class, 0> : QuestionsCorrectForModelBase<Class> {};

#else // !COMBINE_ORDER

template<AnswersClass Class>
struct QuestionsCorrectForModel
{
	Answers<Class> model;
	/**
	 * Offset in vector containing number of correct answers for all students
	 * Exam::nrExams from here are relevant
	 */
	vector<QuestionT>::iterator correctPerExam;

	[[nodiscard]] bool operator<(const QuestionsCorrectForModel &other) const
	{
		for (ExamT nExam = 0; nExam < Exam::nrExams; ++nExam)
		{
			if (correctPerExam[nExam] < other.correctPerExam[nExam]) return true;
			if (correctPerExam[nExam] > other.correctPerExam[nExam]) return false;
		}
		return false;
	}

	[[nodiscard]] bool operator==(const QuestionsCorrectForModel &other) const
	{
		for (ExamT nExam = 0; nExam < Exam::nrExams; ++nExam)
			if (correctPerExam[nExam] != other.correctPerExam[nExam]) return false;
		return true;
	}
};

enum Comparison { Less = -1, Equal = 0, Greater = 1 };

// Compare the combination of left and right with the goal total
Comparison compareQuestionsForModel(
	const vector<QuestionT>::const_iterator left,
	const vector<QuestionT>::const_iterator right,
	const vector<Exam> &total)
{
	for (ExamT nExam = 0; nExam < Exam::nrExams; ++nExam)
	{
		const auto sum = left[nExam] + right[nExam];
		if (sum < total[nExam].questionsCorrect) return Less;
		if (sum > total[nExam].questionsCorrect) return Greater;
	}
	return Equal;
}

#endif // COMBINE_ORDER else



// Result of a run of the algorithm as specified in the exercise
struct Result
{
	AnswersType<Full> possibleSolutions = 0;
	Answers<Full> oneSolution;

	[[nodiscard]] bool operator==(const Result &other) const
	{
		return possibleSolutions == other.possibleSolutions &&
			(possibleSolutions != 1 || oneSolution == other.oneSolution);
	}

	[[nodiscard]] bool operator!=(const Result &other) const { return !(*this == other); }
};

// Print result as specified in the assignment
auto& operator<<(ostream &out, const Result &result)
{
	if (result.possibleSolutions == 1) return out << result.oneSolution;
	return out << result.possibleSolutions << " solutions";
}


#ifdef TEST

// For testing purposes: simple slow full (non-splitted) bruteforce
Result bruteforceFull(const vector<Exam> &exams TRACK_ARG(set<Answers<Full>> &solutions))
{
#ifdef BENCHMARK
	cout << "Starting full bruteforce" << endl;
	const auto startTime = chrono::high_resolution_clock::now();
#endif

	Answers<Full> model;
	Result result;
	while (true)
	{
		for (const auto &exam : exams)
			if (exam.answers.commonBits(model) != exam.questionsCorrect)
				goto nextModel;

		result.oneSolution = model;
		++result.possibleSolutions;

#ifdef TRACK_SOLUTIONS
		solutions.insert(model);
#endif

	nextModel:
		if (model.all()) break;
		++model;
	}

#ifdef BENCHMARK
	{
		const auto endTime = chrono::high_resolution_clock::now();
		cout << "Done in " << chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count() << " ms" << endl;
	}
#endif
	return result;
}

#endif // TEST


/**
 * \param correct Receives number of correct answers in exam for model
 * \return true if we should continue with this model, false if this model should be pruned
 */
template<AnswersClass Class>
bool correctForModel(const Exam &exam, const Answers<Class> model, QuestionT &correct)
{
	correct = getAnswers<Class>(exam).commonBits(model);
#ifdef PRUNE
	return correct <= exam.questionsCorrect
		&& correct + Answers<otherAnswersClass(Class)>::nrQuestions >= exam.questionsCorrect;
#else
	return true;
#endif
}

/**
 * Bruteforce one half
 * \tparam NrChecks Must be equal to max(0, Exam::nrExams - Order::maxFields())
 * \param corrVecs Receives all possible answer models with additional fields
 */
template<
#ifdef COMBINE_ORDER
	ExamT NrChecks,
#endif
	AnswersClass Class
>
void bruteforcePart(const vector<Exam> &exams, vector<QuestionsCorrectForModel<Class COMBINE_ARG(NrChecks)>> &corrVecs)
{
	assert(corrVecs.size() >= size_t{Answers<Class>::mask().data()} + 1);

	Answers<Class> model;
	auto it = corrVecs.begin();
	while (true)
	{
		auto &correctInModel = *it;
		correctInModel.model = model;

#ifdef COMBINE_ORDER
		if constexpr (NrChecks > 0)
		{
			ExamT nExam = 0;

			QuestionT correct;
			if (!correctForModel(exams[nExam], model, correct))
				goto pruneModel; // Sorry, but we had this label lying around anyway

			correctInModel.order.setFirstField(correct);

			while (++nExam < Order::maxFields())
			{
				if (!correctForModel(exams[nExam], model, correct))
					goto pruneModel;

				correctInModel.order.addField(correct);
			}

			for (ExamT nCheck = 0; nCheck < NrChecks; ++nCheck, ++nExam)
			{
				if (!correctForModel(exams[nExam], model, correct))
					goto pruneModel;

				correctInModel.correctPerRemainingExam[nCheck] = correct;
			}
		}
		else
		{
			ExamT nExam = 0;

			QuestionT correct;
			if (!correctForModel(exams[nExam], model, correct))
				goto pruneModel; // Sorry, but we had this label lying around anyway

			correctInModel.order.setFirstField(correct);

			while (++nExam < Exam::nrExams)
			{
				if (!correctForModel(exams[nExam], model, correct))
					goto pruneModel;

				correctInModel.order.addField(correct);
			}
		}

#else // !COMBINE_ORDER
		for (ExamT nExam = 0; nExam < Exam::nrExams; ++nExam)
		{
			QuestionT correct;
			if (!correctForModel(exams[nExam], model, correct))
				goto pruneModel;

			correctInModel.correctPerExam[nExam] = correct;
		}
#endif // COMBINE_ORDER else

		++it;

	pruneModel:
		if (model.all()) break;
		++model;
	}
	corrVecs.resize(it - corrVecs.cbegin());
}


#ifdef COMBINE_ORDER

/**
 * Check for all combinations of lefts and rights starting at leftIt and rightIt with equal left & right orders
 *	if for the remaining exams sum of the the number of correct answers is also equal to the goal sum
 *	and update result accordingly
 * \tparam NrChecks Must be equal to max(0, Exam::nrExams - Order::maxFields())
 * \return true if we should continue; false if we reached the end and should stop
 */
template<ExamT NrChecks>
bool combineEqualsWithBacktracking(
	typename vector<QuestionsCorrectForModel<Left, NrChecks>>::const_iterator &leftIt,
	typename vector<QuestionsCorrectForModel<Right, NrChecks>>::const_iterator &rightIt,
	const typename vector<QuestionsCorrectForModel<Left, NrChecks>>::const_iterator leftEnd,
	const typename vector<QuestionsCorrectForModel<Right, NrChecks>>::const_iterator rightEnd,
	const vector<Exam> &exams, Result &result TRACK_ARG(set<Answers<Full>> &solutions))
{
#ifndef NDEBUG
	const auto fullCorrectOrder = leftIt->order + rightIt->order;
#endif

	const auto firstLeftIt = leftIt;

	while (true)
	{
		assert(leftIt->order + rightIt->order == fullCorrectOrder);
		IFDEB(cout << '=');

		if constexpr (NrChecks > 0)
		{
			const auto &perExamLeft  = leftIt->correctPerRemainingExam;
			const auto &perExamRight = rightIt->correctPerRemainingExam;

			for (ExamT nCheck = 0, nExam = Order::maxFields(); nCheck < NrChecks; ++nCheck, ++nExam)
				if (perExamLeft[nCheck] + perExamRight[nCheck] != exams[nExam].questionsCorrect)
					goto nextModel;
		}

		if (!result.possibleSolutions++)
			result.oneSolution = combineAnswers(leftIt->model, rightIt->model);

		IFDEB(cout << leftIt->model << rightIt->model << endl);

#ifdef TRACK_SOLUTIONS
		{
			const auto inserted = solutions.insert(combineAnswers(leftIt->model, rightIt->model)).second;
#ifdef TEST
			if (!inserted) debugbreak();
#endif
		}
#endif

	nextModel:
		if (const auto nextLeftIt = leftIt + 1;
			nextLeftIt != leftEnd && nextLeftIt->order == leftIt->order)
		{
			IFDEB(cout << "l> ");
			++leftIt; //leftIt = nextLeftIt;
			// orders still equal; repeat
		}
		else // Next left has different order
		{
			const auto oldRightOrder = rightIt->order;
			if (++rightIt == rightEnd) return false;
			IFDEB(cout << "r< ");

			if (rightIt->order == oldRightOrder)
			{
				// Backtrack left
				IFDEB(cout << "l<< ");
				leftIt = firstLeftIt;
				// orders still equal; repeat
			}
			else
			{
				// Optional, but otherwise we would do this on the next iteration
				IFDEB(cout << "L> ");
				++leftIt;
				if (leftIt == leftEnd) return false;

				// orders might not be equal; break inner loop
				break;
			}
		}
	}
	return true;
}

#ifndef TRACK_SOLUTIONS

/**
 * Like combineEqualsWithBacktracking but does not perform any checks; just multiplies differences left & right,
 *	so linear running time
 * \return true if we should continue; false if we reached the end and should stop
 */
bool combineEqualsWithoutBacktracking(
	vector<QuestionsCorrectForModel<Left, 0>>::const_iterator &leftIt,
	vector<QuestionsCorrectForModel<Right, 0>>::const_iterator &rightIt,
	const vector<QuestionsCorrectForModel<Left, 0>>::const_iterator leftEnd,
	const vector<QuestionsCorrectForModel<Right, 0>>::const_iterator rightEnd,
	Result &result)
{
	IFDEB(cout << '=');

	if (!result.possibleSolutions)
		result.oneSolution = combineAnswers(leftIt->model, rightIt->model);

	const auto oldLeftIt = leftIt;
	{
		const auto oldLeftOrder = leftIt->order;
		do ++leftIt;
		while (leftIt != leftEnd && leftIt->order == oldLeftOrder);
	}
	const auto oldRightIt = rightIt;
	{
		const auto oldRightOrder = rightIt->order;
		do ++rightIt;
		while (rightIt != rightEnd && rightIt->order == oldRightOrder);
	}

	IFDEB(cout << 'L' << leftIt - oldLeftIt << "< R" << rightIt - oldRightIt << "< ");

	result.possibleSolutions += (leftIt - oldLeftIt) * (rightIt - oldRightIt);
	return leftIt != leftEnd && rightIt != rightEnd;
}

#endif // !TRACK_SOLUTIONS


/**
 * \tparam NrChecks Must be equal to max(0, Exam::nrExams - Order::maxFields())
 */
template<ExamT NrChecks>
Result combineBruteforcedHalves(
	const vector<QuestionsCorrectForModel<Left, NrChecks>> &corrVecsLeft,
	const vector<QuestionsCorrectForModel<Right, NrChecks>> &corrVecsRight,
	const vector<Exam> &exams, const Order fullCorrectOrder
	TRACK_ARG(set<Answers<Full>> &solutions))
{
	if constexpr (NrChecks > 0)
	IFDEB(cout << "Using combine with backtracking and checks because NrChecks=" << unsigned{NrChecks} << endl);
	else
#ifdef TRACK_SOLUTIONS
	IFDEB(cout << "Using combine with backtracking because of TRACK_SOLUTIONS" << endl);
#else
	IFDEB(cout << "Using combine without backtracking or checks" << endl);
#endif

	Result result;

	auto leftIt  = corrVecsLeft.cbegin();
	auto rightIt = corrVecsRight.cbegin();

	while (true)
	{
		const auto totalOrder = leftIt->order + rightIt->order;
		IFDEB(cout << totalOrder);

		if (totalOrder < fullCorrectOrder)
		{
			IFDEB(cout << "L> ");
			++leftIt;
			if (leftIt == corrVecsLeft.cend()) break;
		}
		else if (totalOrder > fullCorrectOrder)
		{
			IFDEB(cout << "R< ");
			++rightIt;
			if (rightIt == corrVecsRight.cend()) break;
		}
		else // totalOrder == fullCorrectOrder
#ifndef TRACK_SOLUTIONS
			if constexpr (NrChecks > 0)
#else
#pragma message("combineEqualsWithoutBacktracking will never be tested")
#endif
			{
				if (!combineEqualsWithBacktracking<NrChecks>(leftIt, rightIt,
					corrVecsLeft.cend(), corrVecsRight.cend(), exams, result TRACK_ARG(solutions)))
					break;
			}
#ifndef TRACK_SOLUTIONS
			else // NrChecks == 0
				if (!combineEqualsWithoutBacktracking(leftIt, rightIt,
					corrVecsLeft.cend(), corrVecsRight.cend(), result))
					break;
#endif
	}

	return result;
}

#else // !COMBINE_ORDER

Result combineBruteforcedHalves(
	const vector<QuestionsCorrectForModel<Left>> &corrVecsLeft,
	const vector<QuestionsCorrectForModel<Right>> &corrVecsRight,
	const vector<Exam> &exams)
{
	Result result;

	auto leftIt  = corrVecsLeft.cbegin();
	auto rightIt = corrVecsRight.cbegin();

	while (true)
	{
		const auto compare = compareQuestionsForModel(leftIt->correctPerExam, rightIt->correctPerExam, exams);
		switch (compare)
		{
			case Less:
				IFDEB(cout << "L> ");
				++leftIt;
				if (leftIt == corrVecsLeft.cend()) goto end;
				break;

			case Greater:
				IFDEB(cout << "R< ");
				++rightIt;
				if (rightIt == corrVecsRight.cend()) goto end;
				break;

			case Equal:
				IFDEB(cout << '=');

				if (!result.possibleSolutions)
					result.oneSolution = combineAnswers(leftIt->model, rightIt->model);

				const auto oldLeftIt = leftIt;
				{
					const auto &oldLeft = *leftIt;
					do ++leftIt;
					while (leftIt != corrVecsLeft.cend() && *leftIt == oldLeft);
				}
				const auto oldRightIt = rightIt;
				{
					const auto &oldRight = *rightIt;
					do ++rightIt;
					while (rightIt != corrVecsRight.cend() && *rightIt == oldRight);
				}
				IFDEB(cout << 'L' << leftIt - oldLeftIt << "< R" << rightIt - oldRightIt << "< ");

				result.possibleSolutions += (leftIt - oldLeftIt) * (rightIt - oldRightIt);
				if (leftIt == corrVecsLeft.cend() || rightIt == corrVecsRight.cend())
					goto end;
		}
	}

end: return result;
}

#endif // COMBINE_ORDER else



/**
 * \param corrVecsLeft vector that receives correct vectors for the left half (empty)
 * \param corrVecsRight vector that receives correct vectors for the right half (empty)
 * \param correctPerExamHeap vector that receives all correct answer counts (empty)
 */
#ifdef COMBINE_ORDER
template<ExamT NrChecks>
#endif
void bruteforceAndSortHalves(
	const vector<Exam> &exams,
	vector<QuestionsCorrectForModel<Left COMBINE_ARG(NrChecks)>> &corrVecsLeft,
	vector<QuestionsCorrectForModel<Right COMBINE_ARG(NrChecks)>> &corrVecsRight
	NOCOMBINE_ARG(vector<QuestionT> &correctPerExamHeap))
{
#ifdef BENCHMARK
	cout << "Starting bruteforce splitting" << endl;
	auto startTime = chrono::high_resolution_clock::now();
#endif

	corrVecsLeft.resize(size_t{Answers<Left>::mask().data()} + 1);
	corrVecsRight.resize(size_t{Answers<Right>::mask().data()} + 1);

#ifndef COMBINE_ORDER
	correctPerExamHeap.resize((corrVecsLeft.size() + corrVecsRight.size()) * Exam::nrExams);
	{
		auto heapOffset = correctPerExamHeap.begin();
		for (auto leftIt = corrVecsLeft.begin(); leftIt != corrVecsLeft.cend(); ++leftIt, heapOffset += Exam::nrExams)
			leftIt->correctPerExam = heapOffset;
		for (auto rightIt = corrVecsRight.begin(); rightIt != corrVecsRight.cend(); ++rightIt, heapOffset += Exam::nrExams)
			rightIt->correctPerExam = heapOffset;
		assert(heapOffset == correctPerExamHeap.cend());
	}
#endif

	bruteforcePart COMBINE_TEMPL(NrChecks)(exams, corrVecsLeft);
	bruteforcePart COMBINE_TEMPL(NrChecks)(exams, corrVecsRight);

#ifdef BENCHMARK
	{
		const auto endTime = chrono::high_resolution_clock::now();
		cout << "Bruteforced halves in "
			<< chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count()
			<< " ms" << endl;
		startTime = chrono::high_resolution_clock::now();
	}
#endif

	sort(corrVecsLeft.begin(), corrVecsLeft.end());
	sort(corrVecsRight.rbegin(), corrVecsRight.rend()); // Sort in reverse order

#ifdef BENCHMARK
	{
		const auto endTime = chrono::high_resolution_clock::now();
		cout << "Sorted halves in "
			<< chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count()
			<< " ms" << endl;
	}
#endif
}

/**
 * Efficient split-and-combine bruteforce
 * \tparam NrChecks Must be equal to max(0, Exam::nrExams - Order::maxFields())
 */
#ifdef COMBINE_ORDER
template<ExamT NrChecks>
#endif
Result bruteforceSplitAndCombineImpl(const vector<Exam> &exams TRACK_ARG(set<Answers<Full>> &solutions))
{
#ifdef COMBINE_ORDER
	assert(NrChecks == max(0, Exam::nrExams - Order::maxFields()));
#endif

#ifdef BENCHMARK
	const auto startTime = chrono::high_resolution_clock::now();
#endif

	vector<QuestionsCorrectForModel<Left COMBINE_ARG(NrChecks)>> corrVecsLeft;
	vector<QuestionsCorrectForModel<Right COMBINE_ARG(NrChecks)>> corrVecsRight;

#ifndef COMBINE_ORDER
	vector<QuestionT> correctPerExamHeap;
#endif

	bruteforceAndSortHalves COMBINE_TEMPL(NrChecks)(exams, corrVecsLeft, corrVecsRight
		NOCOMBINE_ARG(correctPerExamHeap));

#ifdef COMBINE_ORDER
	Order fullCorrectOrder;
	{
		ExamT nExam = 0;
		fullCorrectOrder.setFirstField(exams[nExam].questionsCorrect);
		for (++nExam; nExam < Exam::nrExams && nExam < Order::maxFields(); ++nExam)
			fullCorrectOrder.addField(exams[nExam].questionsCorrect);
	}

	IFDEB(cout << "\n\n" << fullCorrectOrder << endl);
#endif

	Result result;

	/* It might be that we pruned so much that one side is empty; example input:
	  2 1
	  0 0
	  1 0
	 */
	if (!corrVecsLeft.empty() && !corrVecsRight.empty())
	{
#ifdef BENCHMARK
		const auto startTimeCombine = chrono::high_resolution_clock::now();
#endif

		result = combineBruteforcedHalves COMBINE_TEMPL(NrChecks)(corrVecsLeft, corrVecsRight, exams
			COMBINE_ARG(fullCorrectOrder) TRACK_ARG(solutions));

#ifdef BENCHMARK
		const auto endTime = chrono::high_resolution_clock::now();
		cout << "Combined in "
			<< chrono::duration_cast<chrono::milliseconds>(endTime - startTimeCombine).count()
			<< " ms" << endl;
#endif
	}

#ifdef BENCHMARK
	const auto endTime = chrono::high_resolution_clock::now();
	cout << "Done in "
		<< chrono::duration_cast<chrono::milliseconds>(endTime - startTime).count()
		<< " ms\n" << endl;
#endif
	return result;
}


/**
 * Efficient split-and-combine bruteforce
 */
Result bruteforceSplitAndCombine(const vector<Exam> &exams TRACK_ARG(set<Answers<Full>> &solutions))
{
	Result result;

#ifdef COMBINE_ORDER
	if (Exam::nrExams <= Order::maxFields())
		result = bruteforceSplitAndCombineImpl<0>(exams TRACK_ARG(solutions)); // Omit all checks :D
	else
	{
#define CHECKS_CASE(nrChecks)\
	case nrChecks: result = bruteforceSplitAndCombineImpl<nrChecks>(exams TRACK_ARG(solutions)); break;

		const auto nrChecks = Exam::nrExams - Order::maxFields();
		switch (nrChecks)
		{
			CHECKS_CASE(1);
				// Add cases here, and don't forget to change maxNrChecks below
			default:
				constexpr ExamT maxNrChecks = 2;
				static_assert(maxExams - Order::minMaxFields <= maxNrChecks, "Add check cases");
				assert(nrChecks == maxNrChecks);
				result = bruteforceSplitAndCombineImpl<maxNrChecks>(exams TRACK_ARG(solutions));
		}
	}

#else // !COMBINE_ORDER
	result = bruteforceSplitAndCombineImpl(exams);
#endif // COMBINE_ORDER else
	return result;
}


#ifdef DO_GENERATE
/**
 * \returns seed
 */
auto generateTestCase(ostream &out, const ExamT generateExams, const QuestionT generateQuestions)
{
	out << unsigned{generateExams} << ' ' << unsigned{generateQuestions} << '\n';
	Answers<Full>::nrQuestions = generateQuestions;
	Answers<Full>::updateMask();

#ifdef GENERATE_SEED
	constexpr default_random_engine::result_type seed{GENERATE_SEED};
	default_random_engine random(GENERATE_SEED);
#else
	const default_random_engine::result_type seed =
		chrono::duration_cast<chrono::milliseconds>(chrono::high_resolution_clock::now().
		time_since_epoch()).count();
	default_random_engine random(seed);
#endif
	uniform_int_distribution<AnswersType<Full>> dist(0, Answers<Full>::mask().data());

	const Answers<Full> correctAnswers = dist(random);

	for (unsigned nExam = 0; nExam < generateExams; ++nExam)
	{
		const auto answers = Answers<Full>(dist(random));
		out << answers << ' ' << static_cast<unsigned>(answers.commonBits(correctAnswers)) << '\n';
	}
	return seed;
}
#endif // DO_GENERATE

/**
 * \param exams vector that receives exams (empty)
 */
void readTestCase(istream &in, vector<Exam> &exams)
{
	{
		unsigned nrExams, questions;
		in >> nrExams >> questions;
		assert(nrExams > 0 && nrExams <= maxExams && questions > 0 && questions <= maxQuestions);
		Exam::nrExams              = static_cast<ExamT>(nrExams);
		Answers<Full>::nrQuestions = static_cast<QuestionT>(questions);
	}

#ifdef COMBINE_ORDER
	Order::initBitCounts();
#endif

	Answers<Left>::nrQuestions  = Answers<Full>::nrQuestions / 2;
	Answers<Right>::nrQuestions = Answers<Full>::nrQuestions - Answers<Left>::nrQuestions;

	Answers<Full>::updateMask();
	Answers<Left>::updateMask();
	Answers<Right>::updateMask();

	exams.resize(Exam::nrExams);
	for (auto &exam : exams) in >> exam;
}


int main()
{
#ifdef GENERATE_ALL_COMBINATIONS
	for (ExamT generateExams = 1; generateExams <= maxExams; ++generateExams)
	{
		for (QuestionT generateQuestions = 1; generateQuestions <= maxQuestions; ++generateQuestions)
		{
#endif

#ifdef DO_GENERATE
	// To also test the input reading code
	stringstream in;
	cout << "Seed: " << generateTestCase(in, generateExams, generateQuestions) << '\n'
		<< in.str() << flush;
#else
	auto &in = cin;
#endif

	vector<Exam> exams;
	readTestCase(in, exams);


#ifdef TEST

#ifdef TRACK_SOLUTIONS
	set<Answers<Full>> solutionsFullBruteforce, solutionsSplittedBruteforce;
#endif
	const auto resultFullBruteforce     = bruteforceFull(exams TRACK_ARG(solutionsFullBruteforce)),
	           resultSplittedBruteforce = bruteforceSplitAndCombine(exams TRACK_ARG(solutionsSplittedBruteforce));

	cout << "\n\nFull bruteforce: " << resultFullBruteforce << '\n';
#if defined TRACK_SOLUTIONS && !defined TEST_DONT_PRINT_ALL
	for (const auto model : solutionsFullBruteforce)
	{
		cout << model;
		if (!solutionsSplittedBruteforce.count(model)) cout << " [not in splitted]";
		cout << '\n';
	}
#endif

	cout << "\nSplitted bruteforce: " << resultSplittedBruteforce << '\n';
#if defined TRACK_SOLUTIONS && !defined TEST_DONT_PRINT_ALL
	for (const auto model : solutionsSplittedBruteforce)
	{
		cout << model;
		if (!solutionsFullBruteforce.count(model)) cout << " [not in full]";
		cout << '\n';
	}
#endif

	cout << flush;

	if (resultFullBruteforce != resultSplittedBruteforce) debugbreak();
#ifdef TRACK_SOLUTIONS
	for (const auto model : solutionsFullBruteforce)
		if (!solutionsSplittedBruteforce.count(model)) debugbreak();
#endif

#else // !TEST

#ifdef TRACK_SOLUTIONS
	set<Answers<Full>> solutions;
	cout << bruteforceSplitAndCombine(exams, solutions) << endl;
	for (const auto model : solutions)
		cout << model << '\n';
	cout << flush;
#else
	cout << bruteforceSplitAndCombine(exams) << endl;
#endif
#endif // TEST else

#ifdef GENERATE_ALL_COMBINATIONS
			cout << endl;
		}
	}
#endif
}
